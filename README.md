# Remote Control Motor using ROS

Remote control a motor connected to a raspberry pi using ros from a desktop computer.

## Video

[Video](https://www.youtube.com/watch?v=g0A2d3KKRMI) of moving motor using ROS using code from listener.py

## Setup

### Dependencies to install

common_msgs

### Instructions

Clone to src/ directory of workspace using 

```
git clone git@gitlab.com:ijnek/move-motor-remotely.git
```

on both Desktop and Pi.

1. Ensure those ROS master IP settings are correct in ~/.bashrc on Computer and Pi.
2. Run `catkin_make_isolated` in workspace
3. Run `devel_isolated/setup.bash` in workspace

4. 
    * On Desktop, run `roscore` and `rosrun move-motor-remotely talker.py`
    * On Pi, run `rosrun move-motor-remotely listener.py`