#!/usr/bin/env python

## Simple talker demo that listens to std_msgs/Strings published 
## to the 'chatter' topic

import rospy
from std_msgs.msg import Int16
from sensor_msgs.msg import JointState
import math

from adafruit_servokit import ServoKit

kit = ServoKit(channels=16)
kit.servo[0].set_pulse_width_range(575, 2460)
kit.servo[1].set_pulse_width_range(575, 2460)


class JointMotor:
    def __init__(self, name, controller_index, offset, inverted):
        self.name = name
        self.controller_index = controller_index
        self.offset = offset
        self.inverted = inverted

joints = [
    JointMotor("stllas_robot__pitch_motor_to_arm", 0, math.radians(45), True),
    JointMotor("stllas_robot__roll_motor_to_upper_arm", 1, math.radians(200), True)
]


def callback(data):

    for joint in joints:
        if joint.name in data.name:
            index = data.name.index(joint.name)
            position_rad = data.position[index]
            position_rad *= 1.0 if joint.inverted else -1.0
            position_rad += joint.offset
            position_deg = math.degrees(position_rad)
            kit.servo[joint.controller_index].angle = position_deg

def joint_state_listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber('joint_states', JointState, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    joint_state_listener()
