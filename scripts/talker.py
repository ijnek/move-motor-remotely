#!/usr/bin/env python3
import rospy
from std_msgs.msg import Int16

def talker():
    pub = rospy.Publisher('motor_angle', Int16, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10) # 10hz

    angle = 0
    ascend = True
    while not rospy.is_shutdown():
        if ascend:
            angle += 10
            if angle > 180:
                ascend = False
        else:
            angle -= 10
            if angle < 0:
                ascend = True
        rospy.loginfo(angle)
        pub.publish(angle)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
